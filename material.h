#ifndef MATERIAL_H
#define MATERIAL_H

#include <QImage>
#include <QString>
#include <QVector3D>



class Material
{
public:
    Material();
    const QString mtlName() const;
    void setMtlName(const QString &mtlName);

    const QVector3D diffuseColor() const;
    void setDiffuseColor(const QVector3D &diffuseColor);

    const QVector3D ambienceColor() const;
    void setAmbienceColor(const QVector3D &ambienceColor);

    const QVector3D specularColor() const;
    void setSpecularColor(const QVector3D &specularColor);

    float shinnes() const;
    void setShinnes( float shinnes);

    const QImage diffuseMap() const;
    void setDiffuseMap(const QString &imagePath);

    bool isUseingDiffuseMap() const;

    const QImage normalMap() const;
    void setNormalMap(const QString &normalMapPath);

    bool isUseingNormalMap() const;

    void setDiffuseMap(const QImage &dm);
    void setNormalMap(const QImage &nm);
private:
    QString m_mtlName;
    QVector3D m_diffuseColor;
    QVector3D m_ambienceColor;
    QVector3D m_specularColor;
    float m_shinnes;
    QImage m_diffuseMap;
    QImage m_normalMap;
    bool m_isUseingDiffuseMap;
    bool m_isUseingNormalMap;
};

#endif // MATERIAL_H
