#include "material.h"

Material::Material()
{
    //по умолчанию текстуры не используются
    m_isUseingDiffuseMap = false;
    m_isUseingNormalMap = false;
    //значения материала по умолчанию
    m_shinnes = 90;
    m_diffuseColor  = QVector3D( 1.0,1.0,1.0);//белый рассеяный
    m_ambienceColor = QVector3D( 1.0,1.0,1.0);//белый фоновый
    m_specularColor = QVector3D( 0.5,0.5,0.5);//серый отраженный
}

const QString Material::mtlName() const
{
    return m_mtlName;
}

void Material::setMtlName(const QString &mtlName)
{
    m_mtlName = mtlName;
}

const QVector3D Material::diffuseColor() const
{
    return m_diffuseColor;
}

void Material::setDiffuseColor(const QVector3D &diffuseColor)
{
    m_diffuseColor = diffuseColor;
}

const QVector3D Material::ambienceColor() const
{
    return m_ambienceColor;
}

void Material::setAmbienceColor(const QVector3D &ambienceColor)
{
    m_ambienceColor = ambienceColor;
}

const QVector3D Material::specularColor() const
{
    return m_specularColor;
}

void Material::setSpecularColor(const QVector3D &specularColor)
{
    m_specularColor = specularColor;
}

float Material::shinnes() const
{
    return m_shinnes;
}

void Material::setShinnes(float shinnes)
{
    m_shinnes = shinnes;
}

const QImage Material::diffuseMap() const
{
    return m_diffuseMap;
}

void Material::setDiffuseMap(const QString &imagePath)
{
    m_diffuseMap = QImage(imagePath);
    m_isUseingDiffuseMap = true;
}
void Material::setDiffuseMap(const QImage &dm)
{
    m_diffuseMap = dm;
    m_isUseingDiffuseMap = true;
}

bool Material::isUseingDiffuseMap() const
{
    return m_isUseingDiffuseMap;
}

const QImage Material::normalMap() const
{
    return m_normalMap;
}

void Material::setNormalMap(const QString &normalMapPath)
{
    m_normalMap = QImage(normalMapPath);
    m_isUseingNormalMap = true;
}
void Material::setNormalMap(const QImage &nm)
{
    m_normalMap = nm;
    m_isUseingNormalMap = true;
}

bool Material::isUseingNormalMap() const
{
    return m_isUseingNormalMap;
}
