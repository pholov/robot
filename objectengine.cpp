#include "objectengine.h"

#include <QFile>
#include <QString>
#include <QDebug>
#include <QFileInfo>

//читаем объект из файла
void ObjectEngine::loadFromObj(const QString &path)
{
    QFile ObjFile(path);
    if(!ObjFile.exists())//проверяем, существует ли файл
    {
        qDebug() <<"!!!\nobj file not found\n!!!";
        return ;
    }
    ObjFile.open(QIODevice::ReadOnly);//открываем на чтение

    QTextStream input(&ObjFile);//читаем в поток

    //вектора для считывания в них инф. из файла
    QVector<QVector3D> coords;
    QVector<QVector2D> textureCoords;
    QVector<QVector3D> normals;
    QVector<VertexData> vertexes;
    QVector<uint> indexes;

    Obj3D *object = nullptr;//сначала объект пустой
    QString objectName = "";//без имени
    QString mtlName = "";
    while (!input.atEnd()) {//пока есть символы в файле
        QString line = input.readLine();//читаем новую строку
        QStringList splited = line.split(" ");//разделяем пробелами
        if(splited[0] =="#"){//комментарии, может пригодятся обработать
        }else if(splited[0]=="mtllib")//имя библиотеки материалов (файла .mtl)
        {
            QFileInfo info(path);
            //загружаем библиотеку материалов из mtl файла
            m_materialLibrary.loadMaterialsFromFile(QString("%1/%2").arg(info.absolutePath()).arg(splited[1]));
        }
        else if(splited[0]=="v")//координата вершины
        {
            coords.append(QVector3D(splited[1].toFloat(),splited[2].toFloat(),splited[3].toFloat()));
        }
        else if(splited[0]=="vt"){//координата текстуры
            textureCoords.append(QVector2D(splited[1].toFloat(),splited[2].toFloat()));
       }else if(splited[0]=="vn")//координата нормали
        {
            normals.append(QVector3D(splited[1].toFloat(),splited[2].toFloat(),splited[3].toFloat()));
            }
        else if(splited[0]== "f")//информация о гранях (примитивах)
        {
                for(int i = 1; i<=3; i++)//приметив треугольник (точки три цикл с 1 (в [0] "f") до 3)
                {
                    QStringList vert = splited[i].split("/");//разделяется слешами
                    if(vert[1]=="")//если два слеша подряд, нет координат текстуры
                    {
                        vertexes.append(VertexData(coords[vert[0].toLong()-1],
                                QVector2D(0,0),
                                normals[vert[2].toLong()-1]));

                    }else if(vert.size()==2)//если числа всего два но слеши не подряд, значит нет нормалей
                    {
                        vertexes.append(VertexData(coords[vert[0].toLong()-1],
                                textureCoords[vert[1].toLong()-1],
                                QVector3D(1,1,1)));
                    }else{//иначе записываем все данные
                    vertexes.append(VertexData(coords[vert[0].toLong()-1],
                            textureCoords[vert[1].toLong()-1],
                            normals[vert[2].toLong()-1]));
                    }
                    indexes.append(indexes.size());//увеличиваем массив индексов, примитивы идут последовательно
               }
        }else if(splited[0]=="o"){//имя объекта
            if(object!=nullptr)//если указатель на объект не пустой, созаем объект
            {
                Obj3D *o =new Obj3D(vertexes,indexes,
                                    m_materialLibrary.getMaterialByName(mtlName));
                if(objectName!="")o->name = objectName;
                else o->name = "Default";
                addObject(o);
                objectName = "";
                vertexes.clear();
                indexes.clear();
            }//инициализируем новый
            objectName = splited[1];
            object = new Obj3D;
        }
        else if (splited[0]=="usemtl"){//записываем имя библиотеки
            mtlName = splited[1];

        }
    }
    ObjFile.close();
    Obj3D *o = new Obj3D(vertexes,indexes,m_materialLibrary.getMaterialByName(mtlName));//для последнего объекта
    o->name =objectName;
    addObject(o);

}

void ObjectEngine::addObject(Obj3D *obj)
{
    if(obj==nullptr) return;
    for (int i =0;i<m_objects.size();i++) {
     if(m_objects[i]==obj)
         return;
    }
    m_objects.append(obj);
}



Obj3D *ObjectEngine::getObject(quint32 index)
{
    if(index > m_objects.size()) return nullptr;
    else return m_objects[index];
}

Obj3D *ObjectEngine::getObjectByName(const QString &objectName)
{
    foreach (Obj3D *o, m_objects)
        if(o->name==objectName)
            return o;
    return nullptr;
}

void ObjectEngine::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs){
    foreach(Obj3D *o,m_objects)
        if(o->isVisible())
            o->draw(program,funcs);
}
void ObjectEngine::translate(const QVector3D &translateVector){
    foreach(Obj3D *o,m_objects)
        o->translate(translateVector);
}
void ObjectEngine::rotate (const QQuaternion &rotate){
    foreach(Obj3D *o,m_objects)
        o->rotate(rotate);
}
void ObjectEngine::scale (float scaleX,float scaleY,float scaleZ){
    foreach(Obj3D *o,m_objects)
        o->scale(scaleX,scaleY,scaleZ);
}
void ObjectEngine::setGlobalTransform(const QMatrix4x4 &global){
    foreach(Obj3D *o,m_objects)
        o->setGlobalTransform(global);
}
