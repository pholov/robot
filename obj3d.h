#ifndef OBJ3D_H
#define OBJ3D_H

#include <QOpenGLBuffer>
#include <QMatrix4x4>
#include <QImage>
#include <QQuaternion>
#include "transformational.h"
#include "vertexdatastruct.h"

class QOpenGLTexture;
class QOpenGLFunctions;
class QOpenGLShaderProgram;
class Material;

class Obj3D: Transformational
{
public:
    Obj3D();
    Obj3D(QVector<VertexData> &vertexes, const QVector<GLuint> &indexes, Material * material);
    void draw(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs);
    void translate(const QVector3D &translateVector);
    void rotate (const QQuaternion &rotate);
    void scale (float scaleX,float scaleY,float scaleZ);
    void setGlobalTransform(const QMatrix4x4 &global);
    void hide();
    void show();
    ~Obj3D();

    QString name;
    bool isVisible() const;

    void calculateTBN(QVector<VertexData> &vertData, const QVector<GLuint> indexes);
private:
    QOpenGLTexture *m_diffuseMap;
    QOpenGLTexture *m_normalMap;
    QOpenGLBuffer m_vertexBuffer; //вершинный буфер
    QOpenGLBuffer m_indexBuffer; //индексный буфер

    QQuaternion m_rotate;
    QVector3D m_translate;
    QVector3D m_scale;
    QMatrix4x4 m_globalTransform;
    Material *m_material;
    bool m_isVisible = true;
};

#endif // OBJ3D_H
