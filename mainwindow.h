#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QOpenGLWidget>
#include <GL/gl.h>
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include <QQuaternion>
#include "objectengine.h"


class QOpenGLTexture;
class Obj3D;
class Camera3D;
class SkyBox;
class QOpenGLFramebufferObject;
class Light;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
};

class MyWindow: public QOpenGLWidget
{

private:

protected:
    void initShaders();
    Obj3D * initFloor(float width,
                   float heigth,
                   float depht,
                   QImage *dm=0,//diffuseMap
                   QImage *nm=0);//normalMap
public:
    MyWindow();
    void initializeGL();
    void resizeGL(int nWidth, int nHeight);
    void paintGL();
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void wheelEvent(QWheelEvent *e);
    void keyPressEvent(QKeyEvent *e);

    QVector2D m_mouse_position;
    QMatrix4x4 m_projectionMatrix;//матрица проекций
    QMatrix4x4 m_projectionLightMatrix;
    QOpenGLShaderProgram m_programm;// шейдерная программа
    QOpenGLShaderProgram m_programmDepth;//шейдерная программа для карты теней
    QOpenGLShaderProgram m_programmSkyBox;// шейдерная программа для SkyBox
    QQuaternion m_rotation; // вращение камеры
    QVector<ObjectEngine *> m_objects;
    ObjectEngine * m_objLoader;

    QOpenGLFramebufferObject *m_depthBuffer;
    quint32 m_fbHeight;
    quint32 m_fbWidth;

    SkyBox * m_skybox;
    Camera3D * m_camera;

     Light *m_light[10];
     GLuint m_lightCount = 0;
    ~MyWindow();

};

#endif // MAINWINDOW_H
