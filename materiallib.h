#ifndef MATERIALLIB_H
#define MATERIALLIB_H

#include <QVector>


class Material;

class MaterialLib
{
public:
    MaterialLib();
    void addMaterial(Material * material);
    Material *getMaterialByIndex(quint32 index );
    Material *getMaterialByName(const QString &materialName);
    quint32 countMaterials();
    void loadMaterialsFromFile(const QString &filename);
private:
    void clearLib();
    QVector<Material*> m_materials;
};

#endif // MATERIALLIB_H
