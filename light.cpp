#include "light.h"
#include <QtMath>


Light::Light(const Light::Type &type)//тип, по умолчанию направленный (Directional)
    : m_ambienceColor(1.0f,1.0f,1.0f),//цвет фонового освещения
      m_diffuseColor(1.0f,1.0f,1.0f),//цвет рассеянного освещения
      m_specularColor(1.0f,1.0f,1.0f),//цвет отраженного освещения
      m_position(0.0f,0.0f,0.0f,1.0f),// положение (для точки)
      m_direction(-m_position.toVector3D(),0.0f),//направление (по умолчанию против позиции)
      m_cutoff(M_PI_2)// угол отсечения по умолчанию PI/2

{
    m_type =type;
    m_lightMatrix.setToIdentity();//делаем матрицу единичной
    m_lightMatrix.lookAt(m_position.toVector3D(),//положение источника (откуда)
                        (m_position+m_direction).toVector3D(),//направление (куда)
                         QVector3D(m_direction.x(),m_direction.z(),-m_direction.y()));//"вектор вверх" перпендикулярный плоскости OXY
}
const QVector3D Light::ambienceColor() const
{
    return m_ambienceColor;
}

void Light::setAmbienceColor(const QVector3D &ambienceColor)
{
    m_ambienceColor = ambienceColor;
}

const QVector3D Light::diffuseColor() const
{
    return m_diffuseColor;
}

void Light::setDiffuseColor(const QVector3D &diffuseColor)
{
    m_diffuseColor = diffuseColor;
}

const QVector3D Light::specularColor() const
{
    return m_specularColor;
}

void Light::setSpecularColor(const QVector3D &specularColor)
{
    m_specularColor = specularColor;
}

const QVector4D Light::position() const
{
    return m_position;
}

void Light::setPosition(const QVector4D &position)
{
    m_position = position;
    m_lightMatrix.setToIdentity();
    m_lightMatrix.lookAt(m_position.toVector3D(),
                        (m_position+m_direction).toVector3D(),
                         QVector3D(m_direction.x(),m_direction.z(),-m_direction.y()));
}

const QVector4D Light::direction() const
{
    return m_direction;
}

void Light::setDirection(const QVector4D &direction)
{
    m_direction = direction.normalized();
    if(m_type == Type::Directional)
        m_position = -direction;
    m_lightMatrix.setToIdentity();
    m_lightMatrix.lookAt(m_position.toVector3D(),
                        (m_position+m_direction).toVector3D(),
                         QVector3D(m_direction.x(),m_direction.z(),-m_direction.y()));
}

float Light::cutoff() const
{
    return m_cutoff;
}

void Light::setCutoff(float cutoff)
{
    m_cutoff = cutoff;
}

Light::Type Light::type() const
{
    return m_type;
}

void Light::setType(const Type &type)
{
    m_type = type;
}

const QMatrix4x4 Light::lightMatrix() const
{
    return m_lightMatrix;
}
