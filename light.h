#ifndef LIGHT_H
#define LIGHT_H

#include <QMatrix4x4>
#include <QVector3D>
#include <QVector4D>





class Light {
    public:
    enum Type {
         Directional=0,
         Point=1,
         Spot=2,
     };
    Light(const Type &type = Directional);

    const QVector3D ambienceColor() const;
    void setAmbienceColor(const QVector3D &ambienceColor);

    const QVector3D diffuseColor() const;
    void setDiffuseColor(const QVector3D &diffuseColor);

    const QVector3D specularColor() const;
    void setSpecularColor(const QVector3D &specularColor);

    const QVector4D position() const;
    void setPosition(const QVector4D &position);

    const QVector4D direction() const;
    void setDirection(const QVector4D &direction);

    float cutoff() const;
    void setCutoff(float cutoff);

     Type type() const;
    void setType(const Type &type);

    const QMatrix4x4 lightMatrix() const;

    bool isEnable=true;

private:
    QVector3D m_ambienceColor;
    QVector3D m_diffuseColor;
    QVector3D m_specularColor;
    QVector4D m_position;
    float m_cutoff;
    QVector4D m_direction;
    Type m_type;
    QMatrix4x4 m_lightMatrix;


};

#endif // LIGHT_H
