#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSurfaceFormat format;
    format.setSamples(4);//установка кол-ва семплов на пиксель
    format.setDepthBufferSize(24);//установка буфера глубины
    QSurfaceFormat::setDefaultFormat(format);
    MyWindow *opengl_window = new MyWindow();
    opengl_window->resize(800,600);//масштабирование окна
        opengl_window->show();//вывод окна

    return a.exec();
}
