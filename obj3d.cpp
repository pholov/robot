#include "material.h"
#include "obj3d.h"
#include <QOpenGLTexture>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

Obj3D::Obj3D():
    m_diffuseMap(nullptr),
    m_indexBuffer(QOpenGLBuffer::IndexBuffer)
{
    m_scale = QVector3D (1.0f,1.0f,1.0f);//размер по умолчанию 1
}


Obj3D::Obj3D(QVector<VertexData> &vertexes,
             const QVector<GLuint> &indexes,
            Material *material):
    m_diffuseMap(nullptr),
    m_indexBuffer(QOpenGLBuffer::IndexBuffer)
{

    calculateTBN(vertexes,indexes); //сразу на всякий случай считаем TBN матрицу
    m_scale = QVector3D (1.0f,1.0f,1.0f);

    m_vertexBuffer.create() ;//создаем буфер вершин для вершин
    m_vertexBuffer.bind() ;//биндим его
    //выделяем под него память
    m_vertexBuffer.allocate(vertexes.constData(),vertexes.size()*sizeof(VertexData)) ;//и заполняем
    //освобождаем буфер, пока он не нужен
    m_vertexBuffer.release() ;

    //все тоже самое для индексного буфера
    m_indexBuffer.create();
    m_indexBuffer.bind();
    m_indexBuffer.allocate(indexes.constData(),indexes.size()*sizeof(GLuint));
    m_indexBuffer.release();

    m_material = material;//сохраняем материал

    if(m_material->isUseingDiffuseMap())//если есть текстура
    {
        //создаем текстуру из изображения, её надо отразить из-за противоположного взгляда Qt и OpenGL на направление оси OY
        m_diffuseMap = new QOpenGLTexture(m_material->diffuseMap().mirrored());
        //******************** про фильтры *****************************
        /*
        Текстурные координаты не зависят от разрешения,
        но при этом могут принимать любые значения с плавающей точкой,
        поэтому OpenGL требуется понять какой пиксель текстуры (также называемого текселем) ему требуется наложить.
                */
        m_diffuseMap->setMinificationFilter(QOpenGLTexture::Nearest);//Nearest - выбирает ближайший пиксель
        m_diffuseMap->setMagnificationFilter(QOpenGLTexture::Linear);//Linear - выбирает линейную интерполяцию ближайших пикселей
        m_diffuseMap->setWrapMode(QOpenGLTexture::Repeat);//если текстурные координаты не лежат в промежутке от (0,0) до (1,1) мод Repeat заполнаяет выбраной текстурой
    }
    if(m_material->isUseingNormalMap())//все тоже самое для карты нормалей
    {
        m_normalMap = new QOpenGLTexture(m_material->normalMap().mirrored());
        m_normalMap->setMinificationFilter(QOpenGLTexture::Nearest);
        m_normalMap->setMagnificationFilter(QOpenGLTexture::Linear);
        m_normalMap->setWrapMode(QOpenGLTexture::Repeat);
    }
}

void Obj3D::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs)
{
    //если по какой-то причине буферы пусты, выходим
    if(!m_indexBuffer.isCreated()||!m_vertexBuffer.isCreated()) return;
    if(m_material->isUseingDiffuseMap()){//если используется текстура биндим её на 0 слот и передаём в шейдер
        m_diffuseMap->bind(0);
        program->setUniformValue("u_diffuseMap",0);
    }
    if(m_material->isUseingNormalMap()){//если используется карта нормалей биндим её на 1 слот и передаём в шейдер
        m_normalMap->bind(1);
        program->setUniformValue("u_normalMap",1);
    }
    QMatrix4x4 modelMatrix;//задаем модельную матрицу в связи с характеристиками объекта (смещение, поворот, масштаб)
    modelMatrix.setToIdentity();
    modelMatrix.translate(m_translate);
    modelMatrix.rotate(m_rotate);
    modelMatrix.scale(m_scale);
    modelMatrix = m_globalTransform * modelMatrix;
    program->setUniformValue("u_modelMatrix",modelMatrix);
    //передаем информацию о материале в шейдер
    program->setUniformValue("u_materialProperty.diffuseColor",m_material->diffuseColor());
    program->setUniformValue("u_materialProperty.ambienceColor",m_material->ambienceColor());
    program->setUniformValue("u_materialProperty.specularColor",m_material->specularColor());
    program->setUniformValue("u_materialProperty.shinnes",m_material->shinnes());
    program->setUniformValue("u_isUseingDiffuseMap",m_material->isUseingDiffuseMap());
    program->setUniformValue("u_isUseingNormalMap",m_material->isUseingNormalMap());

    //биндим вершинный буфер, у нас уже есть данные в нем, которые мы загрузили в конструкторе
    m_vertexBuffer.bind();

    int offset = 0;
    //заполняем буфер информацией об объекте
    //позиция вершины
    auto verLoc = program->attributeLocation("a_position");
    program->enableAttributeArray(verLoc);
    program->setAttributeBuffer(verLoc,GL_FLOAT,offset,3,sizeof(VertexData));
    offset += sizeof (QVector3D);
    //текстурные координаты
    auto texLoc = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texLoc);
    program->setAttributeBuffer(texLoc,GL_FLOAT,offset,2,sizeof(VertexData));
    offset += sizeof (QVector2D);
    //нормаль
    auto normLoc = program->attributeLocation("a_normal");
    program->enableAttributeArray(normLoc);
    program->setAttributeBuffer(normLoc,GL_FLOAT,offset,3,sizeof(VertexData));
    offset += sizeof (QVector3D);
    //тангент (касательная)
    auto tangentLoc = program->attributeLocation("a_tangent");
    program->enableAttributeArray(tangentLoc);
    program->setAttributeBuffer(tangentLoc,GL_FLOAT,offset,3,sizeof(VertexData));
    offset += sizeof (QVector3D);
    //битангент (бикасательная)
    auto bitangentLoc = program->attributeLocation("a_bitangent");
    program->enableAttributeArray(bitangentLoc);
    program->setAttributeBuffer(bitangentLoc,GL_FLOAT,offset,3,sizeof(VertexData));
    m_indexBuffer.bind();//биндим индексный буфер (с данными)
    funcs->glDrawElements(GL_TRIANGLES,m_indexBuffer.size(),GL_UNSIGNED_INT,nullptr);//отрисовываем объект треугольниками, используя наши буферы

    //освобождаем буферы;
    m_vertexBuffer.release();
    m_indexBuffer.release();
    if(m_material->isUseingDiffuseMap())
        m_diffuseMap->release();
    if(m_material->isUseingNormalMap())
        m_normalMap->release();

}

void Obj3D::translate(const QVector3D &translateVector)
{
    m_translate += translateVector;
}

void Obj3D::rotate(const QQuaternion &rotate)
{
   m_rotate = rotate * m_rotate;
}

void Obj3D::scale( float scaleX, float scaleY, float scaleZ)
{
    m_scale *= QVector3D(qMax(0.0f,scaleX),qMax(0.0f,scaleY),qMax(0.0f,scaleZ));
}

void Obj3D::setGlobalTransform(const QMatrix4x4 &global)
{
    m_globalTransform = global;
}

void Obj3D::hide()
{
    m_isVisible = false;
}

void Obj3D::show()
{
    m_isVisible = true;
}


Obj3D::~Obj3D()
{
    if(m_vertexBuffer.isCreated())
        m_vertexBuffer.destroy();
    if(m_indexBuffer.isCreated())
        m_indexBuffer.destroy();
    if(m_diffuseMap!=nullptr&&m_diffuseMap->isCreated())
        m_diffuseMap->destroy();
    if(m_normalMap!=nullptr&&m_normalMap->isCreated())
        m_normalMap->destroy();
    if(m_material!=nullptr)
        delete m_material;
}
// расчет TBN для каждой вершины треугольника
// теория о нормалмаппинге и формулы позаимствованны отсюда: https://habr.com/ru/post/415579/
void Obj3D::calculateTBN(QVector<VertexData> &vertData,const QVector<GLuint> indexes)
{
    for(int i = 0; i <  indexes.size(); i+=3 ){
        QVector3D &v1 = vertData[indexes[i]].vd_position;
        QVector3D &v2 = vertData[indexes[i+1]].vd_position;
        QVector3D &v3 = vertData[indexes[i+2]].vd_position;

        QVector2D &uv1 = vertData[indexes[i]].vd_textureCoord;
        QVector2D &uv2 = vertData[indexes[i+1]].vd_textureCoord;
        QVector2D &uv3 = vertData[indexes[i+2]].vd_textureCoord;

        // deltaPos1 = deltaUV1.x * T + deltaUV1.y * B
        //
        // deltaPos2 = deltaUV2.x * T + deltaUV2.y * B
        //
        // T - tangent, B - Bitangent

        QVector3D deltaPos1 = v2 - v1;
        QVector3D deltaPos2 = v3 - v1;

        QVector2D deltaUV1 = uv2 - uv1;
        QVector2D deltaUV2 = uv3 - uv1;

        float x = 1.0f /
                (deltaUV1.x() * deltaUV2.y() - deltaUV1.y() * deltaUV2.x());
        QVector3D tangent = x*(deltaPos1 * deltaUV2.y() - deltaPos2 * deltaUV1.y());
        QVector3D bitangent = x*(deltaPos2 * deltaUV1.x() - deltaPos1 * deltaUV2.x());


            vertData[indexes[i]].vd_tangent = tangent;
            vertData[indexes[i+1]].vd_tangent = tangent;
            vertData[indexes[i+2]].vd_tangent = tangent;
            vertData[indexes[i]].vd_bitangent = bitangent;
            vertData[indexes[i+1]].vd_bitangent = bitangent;
            vertData[indexes[i+2]].vd_bitangent = bitangent;

    }
}

bool Obj3D::isVisible() const
{
    return m_isVisible;
}
