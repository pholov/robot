#include "camera3d.h"

#include <QOpenGLShaderProgram>


Camera3D::Camera3D()
{
    m_scale = QVector3D (1.0f,1.0f,1.0f);
    m_globalTransform.setToIdentity();
}

void Camera3D::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs)
{
    updateViewMatrix();
    program->setUniformValue("u_viewMatrix",m_viewMatrix);

}

void Camera3D::translate(const QVector3D &translateVector)
{
    //чтобы камера перемещалась в развернутых координатах
    // статья про кватернионы https://habr.com/ru/post/255005/
    //ничего не понятно, но работает
    QQuaternion t;
    t.setVector(translateVector);//делаем из вектора кватернион
    t.setScalar(0);//обязательно зануяем w часть
    t = t * m_rotate; // умножаем преобразованный вектор на кватернион вращения камеры
    t =  m_rotate.inverted().normalized() * t; // снова умножаем но наобарот и на кватернион вращения камеры инвертируем и нормируем
    m_translate += t.vector();// смещаем камеру на получившийся вектор
    updateViewMatrix();
}

void Camera3D::rotate(const QQuaternion &rotate)
{
   m_rotate = rotate * m_rotate;
   updateViewMatrix();
}


//чтобы камера при вращении мышью не заваливалась, разделяем вращение по X и Y
void Camera3D::rotateX(const QQuaternion &r)
{
      m_rotateX = r * m_rotateX;//часть поворота по оси X
      m_rotate = m_rotateX*m_rotateY;
      updateViewMatrix();
}

void Camera3D::rotateY(const QQuaternion &r)//часть поворота по оси Y
{
    m_rotateY = r * m_rotateY;
    m_rotate = m_rotateX*m_rotateY;
    updateViewMatrix();
}
//******************************************************************************
void Camera3D::scale( float scaleX, float scaleY, float scaleZ)
{
    m_scale *= QVector3D(qMax(0.0f,scaleX),qMax(0.0f,scaleY),qMax(0.0f,scaleZ));
    updateViewMatrix();
}

void Camera3D::scale(float scaleAll)
{
    m_scale *= scaleAll;
    updateViewMatrix();
}

void Camera3D::setGlobalTransform(const QMatrix4x4 &global)
{
    m_globalTransform = global;
    updateViewMatrix();

}
//обновляем матрицу
void Camera3D::updateViewMatrix()
{    m_viewMatrix.setToIdentity();//затираем старую единичной
     m_viewMatrix.translate(m_zoom);//устанавливаем расстояние прибилжения
     m_viewMatrix.rotate(m_rotate);//поворачиваем (важно)
      m_viewMatrix.translate(m_translate);//перемещаем
     m_viewMatrix.scale(m_scale);//задаем размер
     m_viewMatrix =  m_viewMatrix * m_globalTransform.inverted();//умножаем на глобальные преобразования (изначально единичная)

}

QMatrix4x4 Camera3D::viewMatrix() const
{
    return m_viewMatrix;
}

void Camera3D::clearMovments()
{
    m_translate = {0,1,-3};
    m_zoom = {0,0,0};
    m_scale = {1,1,1};
    m_rotate = QQuaternion::fromAxisAndAngle({1,1,1},0);
    updateViewMatrix();
}

void Camera3D::zoom(float factor)//отдаляем-приближаем камеру
{
    m_zoom += QVector3D(0.0f,0.0f,factor);
    updateViewMatrix();
}

