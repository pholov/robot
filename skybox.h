#ifndef SKYBOX_H
#define SKYBOX_H

#include "transformational.h"

#include <QMatrix4x4>
#include <QQuaternion>

class Obj3D;

class SkyBox : public Transformational
{
public:
    SkyBox(float width,
                   const QString &front,
                   const QString &back,
                   const QString &left,
                   const QString &right,
                   const QString &up,
                   const QString &down);
    void draw(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs);
    void translate(const QVector3D &translateVector);
    void rotate (const QQuaternion &rotate);
    void scale (float scaleX,float scaleY,float scaleZ);
    void setGlobalTransform(const QMatrix4x4 &global);
    ~SkyBox();
private:
    QQuaternion m_rotate;
    QVector3D m_translate;
    QVector3D m_scale;
    QMatrix4x4 m_globalTransform;
    QVector<Obj3D *> m_cube;
};

#endif // SKYBOX_H
