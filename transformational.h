#ifndef TRANSFORMATIONAL_H
#define TRANSFORMATIONAL_H

class QVector3D;
class QQuaternion;
class QMatrix4x4;
class QOpenGLShaderProgram;
class QOpenGLFunctions;

class Transformational
{
public:
    virtual void translate (const QVector3D &t) =0;
    virtual void rotate (const QQuaternion &r) =0;
    virtual void scale (float sx, float sy, float sz) =0;
    virtual void setGlobalTransform(const QMatrix4x4 &g) = 0;
    virtual void draw(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs) = 0;
};

#endif // TRANSFORMATIONAL_H
