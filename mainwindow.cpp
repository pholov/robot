#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVector3D>
#include <QVector2D>
#include <QImage>
#include <QMouseEvent>
#include <QOpenGLContext>
#include <QImageReader>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include "camera3d.h"
#include "obj3d.h"
#include "skybox.h"
#include "objectengine.h"
#include "material.h"
#include "light.h"


MainWindow::~MainWindow()
{
    delete ui;
}
//инициализация шейдеров
//шейдеры написаны на GLSL (OpenGL Shading Language)
//в данной работе используются шейдеры двух типов: вершинные *.vsh и фрагментные (пиксельные) *.fsh
//вершинные предназначены для расчета позиции вершин
//а пиксельные отвечаю за цвет пикселей на экране
void MyWindow::initShaders()
{
    // эта пара шейдеров отвечает за отрисовку объектов в сцене
    if(!m_programm.addShaderFromSourceFile(QOpenGLShader::Vertex,":/v_shader.vsh"))
        close();
    if(!m_programm.addShaderFromSourceFile(QOpenGLShader::Fragment,":/f_shader.fsh"))
        close();
    if(!m_programm.link())
        close();
    //следующая пара отвечает за отрисовку скайбокса
    if(!m_programmSkyBox.addShaderFromSourceFile(QOpenGLShader::Vertex,":/skybox.vsh"))
        close();
    if(!m_programmSkyBox.addShaderFromSourceFile(QOpenGLShader::Fragment,":/skybox.fsh"))
        close();
    if(!m_programmSkyBox.link())
        close();
    //эта пара шейдеров для реализации карты теней (расчитыват глубину)
    if(!m_programmDepth.addShaderFromSourceFile(QOpenGLShader::Vertex,":/depth.vsh"))
        close();
    if(!m_programmDepth.addShaderFromSourceFile(QOpenGLShader::Fragment,":/depth.fsh"))
        close();
    if(!m_programmDepth.link())
        close();
}


Obj3D *MyWindow::initFloor(float width,float heigth,float depht, QImage *dm,QImage *nm)
{
    float width_div_2 = width/2.0f;//т.к. пол находится в центре
    float depht_div_2 = depht/2.0f;//считаем координаты точек, деля ширину и глубину пополам
    QVector<VertexData> vertexes;
    //добавляем вершины
    vertexes.append(VertexData(QVector3D(width_div_2, heigth,depht_div_2),QVector2D(0,16),QVector3D(0,1,0)));
    vertexes.append(VertexData(QVector3D(width_div_2, heigth,-depht_div_2),QVector2D(0,0),QVector3D(0,1,0)));
    vertexes.append(VertexData(QVector3D(-width_div_2,heigth,depht_div_2),QVector2D(16,16),QVector3D(0,1,0)));
    vertexes.append(VertexData(QVector3D(-width_div_2,heigth,-depht_div_2),QVector2D(16,0),QVector3D(0,1,0)));

    QVector<uint> indexes;
    //индексы (прямоугольник рисуется из двух треугольников, т.к. Obj3D отрисовывает модели треугольниками)
    // поэтому иедексов 6
    indexes.append(0);
    indexes.append(1);
    indexes.append(2);
    indexes.append(2);
    indexes.append(1);
    indexes.append(3);
    //********* настройка материала ********
    Material *m = new Material();
    if(dm) m->setDiffuseMap(*dm);
    if(nm) m->setNormalMap(*nm);
    m->setDiffuseColor({0.8f,0.8f,0.8f});
    m->setAmbienceColor({0.8f,0.8f,0.8f});
    m->setSpecularColor({0.0f,0.0f,0.0f});
    //**************************************
    return new Obj3D(vertexes,indexes,m);//возвращаем созданный объект
}


struct robotPartsNames{//структура для названий деталей робота

    QString head;
    QString arms;
    QString body;
    QString legs;
}rpn[2];

MyWindow::MyWindow()
{
    m_fbHeight = 1024;
    m_fbWidth = 1024;//размеры буфера глубины
    m_camera = new Camera3D();//создаем камеру и перемещаем её
    m_camera->rotate(QQuaternion::fromAxisAndAngle(QVector3D(1.0f,0,0),25));
    m_camera->translate(QVector3D(0.0f,-8.0f,-20.0f));
    //указываем имена частей робота, мы точно знаем как они называются ;)
    rpn[0].head ="Ord_Torso";rpn[0].arms="Ord_Arms";
    rpn[0].body="Ord_Mid";rpn[0].legs="Ord_Legs";
    rpn[1].head ="Pat_Torso.002";rpn[1].arms="Pat_Arms.002";
    rpn[1].body="Pat_Mid.002";rpn[1].legs="Pat_Legs.002";
    m_projectionLightMatrix.setToIdentity();//матрица проецирования для тени
    m_projectionLightMatrix.ortho(-90,90, -90,90,-90,90);//в данном случае проекция ортогональная
    m_light[0] = new Light();//направленный источник света
    m_lightCount++;//посчитали источник света
    m_light[0]->setDirection(-QVector4D(10.0f,10.0f,10.0f,0.0f));//направление
    m_light[0]->setDiffuseColor({0.2f,0.2f,0.6f});//цвет
    m_light[1] = new Light(Light::Spot);//прожектор
    m_lightCount++;
    m_light[1]->setPosition(QVector4D(-15.0f,15.0f,15.0f,1.0f));
    m_light[1]->setDirection(QVector4D(15.0f,-15.0f,-15.0f,0.0f));
    m_light[1]->setDiffuseColor({0.0f,0.0f,1.0f});//синий
    m_light[1]->setCutoff(15.0f/180.0f*M_PI);//угол 25 градусов
    m_light[2] = new Light(Light::Point);//точечный источник света
    m_lightCount++;
    m_light[2]->setPosition(QVector4D(20.0f,5.0f,5.0f,1.0f));
    m_light[2]->setDiffuseColor({0.4f,0.0f,0.0f});
}

void MyWindow::initializeGL()
{
    glClearColor(0.0f,0.0f,0.0f,1.0f);//задаем цвет очистки
    glEnable(GL_DEPTH_TEST);//очищаем буфер глубины, он нам понадобится
    glEnable(GL_CULL_FACE);//включаем отображение
    //glCullFace(GL_FRONT);//только лицивых граней
    initShaders();//загрузка шейдеров

    ObjectEngine *oe1 =new ObjectEngine;//создаем обработчик объектов

    QImage dm (":/199.JPG");//карта поглащения света ("текстура")
    QImage nm (":/199_norm.JPG");//карта нормалей
    oe1->addObject(initFloor(30.0f,0.0f,30.0f,
              &dm,&nm));//создаем пол и добавляем его в обработчик объектов

    oe1->loadFromObj(":/Pat/Pat/Pat.obj");//загружаем модель первого робота
    oe1->loadFromObj(":/Ord/Ord/Ord2.obj");//и второго
    //прячем детали второго робота
    oe1->getObjectByName("Ord_Torso")->hide();
    oe1->getObjectByName("Ord_Arms")->hide();
    oe1->getObjectByName("Ord_Mid")->hide();
    oe1->getObjectByName("Ord_Legs")->hide();
    m_objects.append(oe1);//сохраняем обработчик объектов в переменной класа

    QString pref = ":/blue/blue/bkg1_";//начало пути к текстурам скайбокса; blue папка в папке с программой, bkg1_ начало имени текстуры
    m_skybox = new SkyBox(99999,
                          pref+"front.png",pref+"back.png",
                          pref+"left.png", pref+"right.png"
                         ,pref+"top.png",  pref+"bot.png");//создаем скайбокс
    m_depthBuffer = new QOpenGLFramebufferObject(m_fbWidth,m_fbHeight,QOpenGLFramebufferObject::Depth);//создаем буфер глубины

}
void MyWindow::resizeGL(int nWidth, int nHeight)
{
    float aspect = nWidth/(nHeight ? float(nHeight) : 1);//считаем отношение сторон
    //приводим проекционную матрицу к единичной
    m_projectionMatrix.setToIdentity();
    //масштабирум окно
    m_projectionMatrix.perspective(45/*угол поля зрения в направлении OY*/,
                                   aspect/*отношение сторон в направлении OX*/,
                                   0.01f/*насколько близко отрисовывать*/
                                   ,100000.0f/*насколько далеко отрисовывать*/);

}
void MyWindow::paintGL()
{

    //отрисовка во фрэймбуффер (для карты теней)
    m_depthBuffer->bind();//биндим буфер, мы его уже объявляли
    glViewport(0,0,m_fbWidth,m_fbHeight);//устанавливаем прот просмотра (от (0,0 до 1024,1024)
    glClear(GL_DEPTH_BUFFER_BIT |GL_COLOR_BUFFER_BIT);//очищаем глубину и цвет
    m_programmDepth.bind();//биндим шейдерную программу
    //******************* устанавливаем значения uniform переменных ********************
    m_programmDepth.setUniformValue("u_projectionLightMatrix",m_projectionLightMatrix);
    m_programmDepth.setUniformValue("u_shadowLightMatrix",m_light[0]->lightMatrix());
    //**********************************************************************************

    //в цикле отрисовываем каждый объект в сцене (кроме скайбокса и камеры, разумеется),
    //чтобы получить тень
    foreach (ObjectEngine * obj, m_objects) {
        obj->draw(&m_programmDepth, context()->functions());
    }
    m_programmDepth.release();//освобождаем программу
    m_depthBuffer->release();//очищаем буфер глубины

    GLuint texture = m_depthBuffer->texture();//получаем id текстуры
    //делаем текстуру активной
    context()->functions()->glActiveTexture(GL_TEXTURE6);//используем контекст, т.к. по какой-то причине эта ф-ция у нас не определена
    glBindTexture(GL_TEXTURE_2D, texture);//биндим текстуру на GL_TEXTURE6

    //отрисовка на экран
    glViewport(0,0,width(),height());//устанавливаем нормальный порт просмотра в координатах окна
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    m_programmSkyBox.bind();//биндим программу отрисовки скайбокса
    //рисуем скайбокс
    m_camera->draw(&m_programmSkyBox);
    m_programmSkyBox.setUniformValue("u_projectionMatrix",m_projectionMatrix);
    m_skybox->draw(&m_programmSkyBox,context()->functions());
    m_programmSkyBox.release();


    m_programm.bind();//биндим основную программу
    m_camera->draw(&m_programm);//ставим камеру
    //******************* устанавливаем значения uniform переменных ********************
    m_programm.setUniformValue("u_projectionLightMatrix",m_projectionLightMatrix);
    m_programm.setUniformValue("u_shadowMap",GL_TEXTURE6-GL_TEXTURE0);//нужно передать id, а он считается какGL_TEXTUREX-GL_TEXTURE0
    m_programm.setUniformValue("u_projectionMatrix",m_projectionMatrix);
        m_programm.setUniformValue("u_shadowLightMatrix", m_light[0]->lightMatrix());
        m_programm.setUniformValue("u_lightPower",3.0f);
        m_programm.setUniformValue("u_specularColor",QVector4D(1.0f,1.0f,1.0f,1.0f));
   //**********************************************************************************
   //******************* устанавливаем значения uniform переменных для разных источников света ********************
     int lCount = 0;
    for(int i = 0; i<m_lightCount;i++){
        m_programm.setUniformValue(QString("u_lightProperty[%1].ambienceColor").arg(i).toLatin1().data(),m_light[i]->ambienceColor());
        m_programm.setUniformValue(QString("u_lightProperty[%1].diffuseColor").arg(i).toLatin1().data(),m_light[i]->diffuseColor());
        m_programm.setUniformValue(QString("u_lightProperty[%1].specularColor").arg(i).toLatin1().data(),m_light[i]->specularColor());
        m_programm.setUniformValue(QString("u_lightProperty[%1].position").arg(i).toLatin1().data(),m_light[i]->position());
        m_programm.setUniformValue(QString("u_lightProperty[%1].direction").arg(i).toLatin1().data(),m_light[i]->direction());
        m_programm.setUniformValue(QString("u_lightProperty[%1].cutoff").arg(i).toLatin1().data(),m_light[i]->cutoff());//угол отсечения
        m_programm.setUniformValue(QString("u_lightProperty[%1].type").arg(i).toLatin1().data(),m_light[i]->type());//тип
        m_programm.setUniformValue(QString("u_lightProperty[%1].isEnable").arg(i).toLatin1().data(),m_light[i]->isEnable);//включен ли
        lCount++;
    }
    //*************************************************************************************************************
    m_programm.setUniformValue("u_lightCount",lCount);//кол-во источников света

    //рисуем объекты в сцену

    foreach (ObjectEngine * obj, m_objects) {
        obj->draw(&m_programm, context()->functions());
    }
    m_programm.release();//освобождаем программу
}


//нажатия кнопок мыши
void MyWindow::mousePressEvent(QMouseEvent *e)
{
    if(e->buttons()==Qt::LeftButton){//если нажали левую кнопку мыши
        m_mouse_position = QVector2D( e->localPos()); //сохраняем координаты
    e->accept();
    }
}

//перемещения мыши
void MyWindow::mouseMoveEvent(QMouseEvent *e)
{
 if(e->buttons()!=Qt::LeftButton) return;//если не нажата левая кнопак, выходим
 QVector2D diff = QVector2D( e->localPos()) - m_mouse_position; //считаем сколько прошла мышь на экране
 m_mouse_position = QVector2D( e->localPos());//обновляем данные о позиции мыши

 float angleX = diff.y()/5.0f;//угол поворота по X определяем как y вектора перемещения мыши
 float angleY = diff.x()/5.0f;//для Y наооборот
 //на 5 делим чтобы камера не дергалась

 m_camera->rotateX(QQuaternion::fromAxisAndAngle(1.0f,0,0,angleX));//поворачиваем камеру по X
 m_camera->rotateY(QQuaternion::fromAxisAndAngle(0,1.0f,0,angleY));//поворачиваем камеру по Y
 update();//перерисовываем окно
}
//колесеко мыши
void MyWindow::wheelEvent(QWheelEvent *e)
{
    float scaleFactor = 0.3f;//на сколько приближать(отдалять)
    if(e->delta()>0)//если колесеко крутим на себя
    {
        m_camera->zoom(scaleFactor);//приближаем
    }else if(e->delta()<0)//если колесеко крутим от себя
    {
       m_camera->zoom(-scaleFactor);//отдаляем
    }
    update();//перерисовываем окно
}

bool i[4]={1,1,1,1};//массив значений деталь какого робота отображать 1 - Pat; 0 - Ord

//нажатие клавишь на клавиатуре
void MyWindow::keyPressEvent(QKeyEvent *e)
{
    float angle = 2.0f;//угол поворота камеры
    int k = e->key();//какую клавишу нажали
    switch (k) {
    case Qt::Key_W: {m_camera->translate(QVector3D(0,0,1));break;}//перемещаемся вперед
    case Qt::Key_S: {m_camera->translate(QVector3D(0,0,-1));break;}//перемещаемся назад
    case Qt::Key_A: {m_camera->translate(QVector3D(1,0,0));break;}//перемещаемся вправо
    case Qt::Key_D: {m_camera->translate(QVector3D(-1,0,0));break;}//перемещаемся влево
    case Qt::Key_E:{m_camera->translate(QVector3D(0,1,0));break;}//перемещаемся вниз
    case Qt::Key_Q:{m_camera->translate(QVector3D(0,-1,0));break;}//перемещаемся вверх
    case Qt::Key_F:{//возвращаем камеру в исходное положение
        m_camera->clearMovments();
        //как в конструкторе
        m_camera->rotate(QQuaternion::fromAxisAndAngle(QVector3D(1.0f,0,0),25));
        m_camera->translate(QVector3D(0.0f,-8.0f,-20.0f));
        break;}
    case Qt::Key_1: {m_objects.last()->getObjectByName(rpn[i[0]].head)->hide();
        m_objects.last()->getObjectByName(rpn[!i[0]].head)->show();i[0]=!i[0];break;}//рисуем голову робота если i=0, то Pat'а и инвертирум значение i (i=1)
    case Qt::Key_2: {m_objects.last()->getObjectByName(rpn[i[1]].arms)->hide();      // если i=1, то Ord'a и i=0;
        m_objects.last()->getObjectByName(rpn[!i[1]].arms)->show();i[1]=!i[1];break;}//и так для каждой части тела, rpn - массив структур с именами деталей
    case Qt::Key_3: {m_objects.last()->getObjectByName(rpn[i[2]].body)->hide();
        m_objects.last()->getObjectByName(rpn[!i[2]].body)->show();i[2]=!i[2];break;}
    case Qt::Key_4: {m_objects.last()->getObjectByName(rpn[i[3]].legs)->hide();
        m_objects.last()->getObjectByName(rpn[!i[3]].legs)->show();i[3]=!i[3];break;}
    case Qt::Key_5: {m_light[0]->isEnable=!m_light[0]->isEnable;break;} // вкл/выкл направленый свет
    case Qt::Key_6: {m_light[1]->isEnable=!m_light[1]->isEnable;break;}// вкл/выкл прожектор
    case Qt::Key_7: {m_light[2]->isEnable=!m_light[2]->isEnable;break;}// вкл/выкл точечный свет
        //повороты камеры стрелками
    case Qt::Key_Left:
    {
        m_camera->rotateY(QQuaternion::fromAxisAndAngle(0,1.0f,0,-angle));
        break;
    }
    case Qt::Key_Right:
    {
        m_camera->rotateY(QQuaternion::fromAxisAndAngle(0,1.0f,0,angle));
        break;
    }    case Qt::Key_Up:
    {
        m_camera->rotateX(QQuaternion::fromAxisAndAngle(1.0f,0,0,-angle));
        break;
    }
    case Qt::Key_Down:
    {
        m_camera->rotateX(QQuaternion::fromAxisAndAngle(1.0f,0,0,angle));
        break;
    }
    default:;
    }
    update();//перерисовываем
}

MyWindow::~MyWindow()
{
    delete m_camera;
    foreach (ObjectEngine * o, m_objects) {
        delete o;
    }
}
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}
