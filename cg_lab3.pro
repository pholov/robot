#-------------------------------------------------
#
# Project created by QtCreator 2019-04-15T08:20:13
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = robot
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 resources_big

SOURCES += \
        camera3d.cpp \
        light.cpp \
        main.cpp \
        mainwindow.cpp \
        material.cpp \
        materiallib.cpp \
        obj3d.cpp \
        objectengine.cpp \
        skybox.cpp

HEADERS += \
        camera3d.h \
        light.h \
        mainwindow.h \
        material.h \
        materiallib.h \
        obj3d.h \
        objectengine.h \
        skybox.h \
        transformational.h \
        vertexdatastruct.h
FORMS += \
        mainwindow.ui

unix: {
    LIBS += -L/usr/lib64/ -lGL -L/usr/lib64/ -lGLU
}
win32: {
    LIBS += -lopengl32
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

RESOURCES += \
    blue.qrc \
    ord.qrc \
    pat.qrc \
    resource.qrc
