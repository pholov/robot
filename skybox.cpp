#include "skybox.h"
#include "obj3d.h"
#include "material.h"

SkyBox::SkyBox(float width,
               const QString &front,
               const QString &back,
               const QString &left,
               const QString &right,
               const QString &up,
               const QString &down)
{

    //здесь какая-то черная магия, работает странно, но работает
    float width_div_2 = width / 2.0f;

    QVector<VertexData> vertexes;
    QVector<GLuint> indexes = {0,2,1,2,3,1};
    QVector<VertexData> v1;
    Material *mtl = new Material;

    QString names[6] = {back,right,up,front,left,down};
    mtl->setDiffuseMap(front);

    v1.append(VertexData(QVector3D(-width_div_2, width_div_2, width_div_2),
                                 QVector2D(1.0f, 1.0f), QVector3D(0.0f, 0.0f, -1.0f)));
    v1.append(VertexData(QVector3D(-width_div_2, -width_div_2, width_div_2),
                                 QVector2D(1.0f, 0.0f), QVector3D(0.0f, 0.0f, -1.0f)));
    v1.append(VertexData(QVector3D(width_div_2, width_div_2, width_div_2),
                                QVector2D(0.0f, 1.0f), QVector3D(0.0f, 0.0f, -1.0f)));
    v1.append(VertexData(QVector3D(width_div_2, -width_div_2, width_div_2),
                               QVector2D(0.0f, 0.0f), QVector3D(0.0f, 0.0f, -1.0f)));
    m_cube.append(new Obj3D(v1, indexes, mtl));
    QVector<VertexData> v2;
    mtl->setDiffuseMap(left);
    v2.append(VertexData(QVector3D(width_div_2, width_div_2, width_div_2),
                               QVector2D(1.0f, 1.0f), QVector3D(-1.0f, 0.0f, 0.0f)));
    v2.append(VertexData(QVector3D(width_div_2, -width_div_2, width_div_2),
                               QVector2D(1.0f, 0.0f), QVector3D(-1.0f, 0.0f, 0.0f)));
    v2.append(VertexData(QVector3D(width_div_2, width_div_2, -width_div_2),
                               QVector2D(0.0f, 1.0f), QVector3D(-1.0f, 0.0f, 0.0f)));
    v2.append(VertexData(QVector3D(width_div_2, -width_div_2, -width_div_2),
                               QVector2D(0.0f, 0.0f), QVector3D(-1.0f, 0.0f, 0.0f)));
    m_cube.append(new Obj3D(v2, indexes, mtl));
    QVector<VertexData> v3;
    mtl->setDiffuseMap(up);
    v3.append(VertexData(QVector3D(width_div_2, width_div_2, width_div_2),
                               QVector2D(0.0f, 0.0f), QVector3D(0.0f, -1.0f, 0.0f)));
    v3.append(VertexData(QVector3D(width_div_2, width_div_2, -width_div_2),
                               QVector2D(0.0f, 1.0f), QVector3D(0.0f, -1.0f, 0.0f)));
    v3.append(VertexData(QVector3D(-width_div_2, width_div_2, width_div_2),
                               QVector2D(1.0f, 0.0f), QVector3D(0.0f, -1.0f, 0.0f)));
    v3.append(VertexData(QVector3D(-width_div_2, width_div_2, -width_div_2),
                               QVector2D(1.0f, 1.0f), QVector3D(0.0f, -1.0f, 0.0f)));
    m_cube.append(new Obj3D(v3, indexes, mtl));
    QVector<VertexData> v4;
    mtl->setDiffuseMap(back);
    v4.append(VertexData(QVector3D(width_div_2, width_div_2, -width_div_2),
                               QVector2D(1.0f, 1.0f), QVector3D(0.0f, 0.0f, 1.0f)));
    v4.append(VertexData(QVector3D(width_div_2, -width_div_2, -width_div_2),
                               QVector2D(1.0f, 0.0f), QVector3D(0.0f, 0.0f, 1.0f)));
    v4.append(VertexData(QVector3D(-width_div_2, width_div_2, -width_div_2),
                               QVector2D(0.0f, 1.0f), QVector3D(0.0f, 0.0f, 1.0f)));
    v4.append(VertexData(QVector3D(-width_div_2, -width_div_2, -width_div_2),
                               QVector2D(0.0f, 0.0f), QVector3D(0.0f, 0.0f, 1.0f)));
    m_cube.append(new Obj3D(v4, indexes, mtl));
    QVector<VertexData> v5;
    mtl->setDiffuseMap(right);
    v5.append(VertexData(QVector3D(-width_div_2, width_div_2, width_div_2),
                               QVector2D(0.0f, 1.0f), QVector3D(1.0f, 0.0f, 0.0f)));
    v5.append(VertexData(QVector3D(-width_div_2, width_div_2, -width_div_2),
                               QVector2D(1.0f, 1.0f), QVector3D(1.0f, 0.0f, 0.0f)));
    v5.append(VertexData(QVector3D(-width_div_2, -width_div_2, width_div_2),
                               QVector2D(0.0f, 0.0f), QVector3D(1.0f, 0.0f, 0.0f)));
    v5.append(VertexData(QVector3D(-width_div_2, -width_div_2, -width_div_2),
                               QVector2D(1.0f, 0.0f), QVector3D(1.0f, 0.0f, 0.0f)));
    m_cube.append(new Obj3D(v5, indexes, mtl));
    QVector<VertexData> v6;
    mtl->setDiffuseMap(down);
    v6.append(VertexData(QVector3D(-width_div_2, -width_div_2, width_div_2),
                               QVector2D(1.0f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f)));
    v6.append(VertexData(QVector3D(-width_div_2, -width_div_2, -width_div_2),
                               QVector2D(1.0f, 0.0f), QVector3D(0.0f, 1.0f, 0.0f)));
    v6.append(VertexData(QVector3D(width_div_2, -width_div_2, width_div_2),
                               QVector2D(0.0f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f)));
    v6.append(VertexData(QVector3D(width_div_2, -width_div_2, -width_div_2),
                               QVector2D(0.0f, 0.0f), QVector3D(0.0f, 1.0f, 0.0f)));
    m_cube.append(new Obj3D(v6, indexes, mtl));

    //Material *mtl[6];
//    Material *mtl = new Material;
//    mtl->setDiffuseMap(back);
//    QString names[6] = {back,right,up,front,left,down};
//    for(GLuint i = 0; i < 6; i++){
//        m_cube.append(new Obj3D(vertexes[i], indexes[i], mtl));
//    }

}

void SkyBox::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs)
{
    foreach(Obj3D *c,m_cube)
        c->draw(program,funcs);
}

void SkyBox::translate(const QVector3D &translateVector)
{

}

void SkyBox::rotate(const QQuaternion &rotate)
{

}

void SkyBox::scale(float scaleX, float scaleY, float scaleZ)
{

}

void SkyBox::setGlobalTransform(const QMatrix4x4 &global)
{

}

SkyBox::~SkyBox(){
    foreach(Obj3D *c,m_cube)
    {
        delete c;
        }
}
