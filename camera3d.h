#ifndef CAMERA3D_H
#define CAMERA3D_H

#include "transformational.h"

#include <QMatrix4x4>
#include <QQuaternion>

class Camera3D : public Transformational
{
public:
    Camera3D();
    void draw(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs=0);//установка камеры
    void translate(const QVector3D &translateVector);//перемещение камеры
    void rotate (const QQuaternion &rotate);//поворот камеры
    void rotateX (const QQuaternion &rotate);//поворот вокруг X
    void rotateY (const QQuaternion &rotate);//поворот вокруг Y
    void scale (float scaleX,float scaleY,float scaleZ);//изменение размера
    void scale (float scaleAll);
    void setGlobalTransform(const QMatrix4x4 &global);//установка матрицы глобальных преобразований
    void updateViewMatrix();// обновить видовую матрицу
    QMatrix4x4 viewMatrix() const;// получить видовую матрицу
    void clearMovments();//удальить перемещения (поставить в точку (0,0,0))

    void zoom(float factor);//приблизить-отдалить
private:
    QMatrix4x4 m_viewMatrix;//видовая матрица
    QQuaternion m_rotate;//поворот (суммарный кватернион)
    QQuaternion m_rotateX;//поворот вокруг X
    QQuaternion m_rotateY;//поворот вокруг Y
    QVector3D m_translate;//перемещение (суммарный вектор)
    QVector3D m_scale;// суммарный размер
    QVector3D m_zoom;// суммарное приближение
    QMatrix4x4 m_globalTransform;//матрица глобальных преобразований

};

#endif // CAMERA3D_H
